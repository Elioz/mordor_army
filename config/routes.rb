Rails.application.routes.draw do
  resources :personnages
  resources :grades
  resources :races
  get 'home/index'

  root 'home#index'
end
