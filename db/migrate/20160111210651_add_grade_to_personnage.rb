class AddGradeToPersonnage < ActiveRecord::Migration
  def change
    add_reference :personnages, :grade, index: true, foreign_key: true
  end
end
