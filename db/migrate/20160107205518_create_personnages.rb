class CreatePersonnages < ActiveRecord::Migration
  def change
    create_table :personnages do |t|
      t.string :nom
      t.string :titre
      t.integer :niveau
      t.integer :hp
      t.integer :attaque

      t.timestamps null: false
    end
  end
end
