class AddRaceToPersonnage < ActiveRecord::Migration
  def change
    add_reference :personnages, :race, index: true, foreign_key: true
  end
end
