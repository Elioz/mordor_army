class HomeController < ApplicationController
  def index
  	@personnages = Personnage.getPerso
  	@grades = Grade.getGrade
  end
end
