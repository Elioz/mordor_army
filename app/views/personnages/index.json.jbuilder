json.array!(@personnages) do |personnage|
  json.extract! personnage, :id, :nom, :titre, :niveau, :hp, :attaque
  json.url personnage_url(personnage, format: :json)
end
